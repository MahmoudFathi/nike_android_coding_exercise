package com.example.nikecodeassessment.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.nikecodeassessment.AppController
import com.example.nikecodeassessment.R
import com.example.nikecodeassessment.adapters.DataAdapter
import com.example.nikecodeassessment.common.hideKeyboard
import com.example.nikecodeassessment.common.snackbar
import com.example.nikecodeassessment.di.activity.ActivityModule
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var mainViewModel: MainViewModel


    lateinit var myAdapter: DataAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as AppController).applicationComponent.newActivityComponent(ActivityModule(this)).inject(this)

        if(savedInstanceState != null){
            etTerm.setText(savedInstanceState.getString("EditText"))
            mainViewModel.livedata.observe(this@MainActivity, Observer {
                myAdapter = DataAdapter(it)
                pb.visibility = View.GONE
                rvSearch.apply {
                    addItemDecoration(DividerItemDecoration(this.context, RecyclerView.VERTICAL))
                    adapter = myAdapter
                    layoutManager = LinearLayoutManager(this.context)
                }
            })
        }

        hideKeyboard(etTerm)

        pb.visibility = View.GONE
        btThumbsUp.setOnClickListener {
            if(mainViewModel.loadDataMostUp()){
                myAdapter.notifyDataSetChanged()
            }else{
                snackbar("Nothing to Sort", "Close", coordinator)
            }

        }

        btThumbsDown.setOnClickListener {
            if(mainViewModel.loadDataMostDown()){
                myAdapter.notifyDataSetChanged()
            }else{
                snackbar("Nothing to Sort", "Close", coordinator)
            }

        }

        btSearch.setOnClickListener {
            hideKeyboard(etTerm)
            val input = etTerm.text.toString().trim()
            if(input.isNullOrBlank()){
                snackbar("Empty Text","Close",coordinator)
            }else{
                pb.visibility = View.VISIBLE
                mainViewModel.loadData(input)
                mainViewModel.livedata.observe(this@MainActivity, Observer {

                    myAdapter = DataAdapter(it)
                    pb.visibility = View.GONE
                    rvSearch.apply {
                        addItemDecoration(DividerItemDecoration(this.context, RecyclerView.VERTICAL))
                        adapter = myAdapter
                        layoutManager = LinearLayoutManager(this.context)
                        }
                })
            }
        }

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putString("EditText",etTerm.text.toString())
    }




}




