package com.example.nikecodeassessment.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nikecodeassessment.data.repository.Repository
import com.example.nikecodeassessment.model.X
import kotlinx.coroutines.launch

/**
 * Updates MainActivity with a list of Words
 */
class MainViewModel(private val repository: Repository, private val liveData: MutableLiveData<List<X>>):ViewModel() {
    val livedata:LiveData<List<X>> = liveData
    var apiCalled = false

    fun loadData(term:String){
        viewModelScope.launch {
            val res = repository.getDataAsync(term)
            liveData.value = res.list
            apiCalled = true
        }
    }

    fun loadDataMostUp(): Boolean = if (apiCalled) {
        liveData.value = liveData.value?.sortedByDescending {
            it.thumbsUp
        }
        true
    }else{
        false
    }

    fun loadDataMostDown(): Boolean = if (apiCalled) {
        liveData.value = liveData.value?.sortedByDescending {
            it.thumbsDown
        }
        true
    }else{
        false
    }

}