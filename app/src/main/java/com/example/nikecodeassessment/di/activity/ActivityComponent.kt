package com.example.nikecodeassessment.di.activity

import com.example.nikecodeassessment.factories.MainViewModelFactory
import com.example.nikecodeassessment.ui.MainActivity
import dagger.Subcomponent
/**
 * Individual component for activity
 */
@ActivityScope
@Subcomponent(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(mainActivity: MainActivity)

    fun getMainViewModelFactory():MainViewModelFactory
}