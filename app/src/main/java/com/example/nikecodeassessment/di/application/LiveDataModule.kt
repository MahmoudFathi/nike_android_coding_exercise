package com.example.nikecodeassessment.di.application

import androidx.lifecycle.MutableLiveData
import com.example.nikecodeassessment.model.X
import dagger.Module
import dagger.Provides

@Module
class LiveDataModule {
    @Provides
    @ApplicationScope
    fun provideLiveData()= MutableLiveData<List<X>>()

}