package com.example.nikecodeassessment.di.activity

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.nikecodeassessment.factories.MainViewModelFactory
import com.example.nikecodeassessment.ui.MainViewModel
import dagger.Module
import dagger.Provides
/**
 * Instantiates activity specific dependencies
 */
@Module
class ActivityModule(val activity: AppCompatActivity) {

    @Provides
    @ActivityScope
    fun provideMainViewModel(vf: MainViewModelFactory)= ViewModelProviders.of(activity,vf).get(MainViewModel::class.java)
}