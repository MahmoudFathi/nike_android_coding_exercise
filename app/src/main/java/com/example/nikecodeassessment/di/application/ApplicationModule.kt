package com.example.nikecodeassessment.di.application

import android.content.Context
import com.example.nikecodeassessment.data.remote.ApiCall
import com.example.nikecodeassessment.data.repository.Repository
import com.example.nikecodeassessment.data.repository.RepositoryImpl
import dagger.Module
import dagger.Provides
/**
 * Instantiates application wide dependencies
 */
@Module
class ApplicationModule(val context: Context) {

    @Provides
    @ApplicationScope
    fun provideApiCall()= ApiCall.create(context.applicationContext)

    @Provides
    @ApplicationScope
    fun provideRepository(apiCall: ApiCall): Repository = RepositoryImpl(apiCall)
}