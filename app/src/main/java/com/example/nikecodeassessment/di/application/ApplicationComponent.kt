package com.example.nikecodeassessment.di.application

import com.example.nikecodeassessment.di.activity.ActivityComponent
import com.example.nikecodeassessment.di.activity.ActivityModule
import dagger.Component
/**
 * Component for the application instance
 */
@ApplicationScope
@Component(modules = [ApplicationModule::class,LiveDataModule::class])
interface ApplicationComponent {
    fun newActivityComponent(activityModule: ActivityModule): ActivityComponent

}