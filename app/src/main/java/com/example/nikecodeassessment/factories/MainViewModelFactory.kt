package com.example.nikecodeassessment.factories

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.nikecodeassessment.data.repository.Repository
import com.example.nikecodeassessment.model.X
import com.example.nikecodeassessment.ui.MainViewModel
import java.lang.IllegalArgumentException
import javax.inject.Inject
/**
 * Factory for MainViewModel
 */
class MainViewModelFactory @Inject constructor(val repository: Repository,val liveData: MutableLiveData<List<X>>):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return if(modelClass.isAssignableFrom(MainViewModel::class.java))MainViewModel(repository,liveData)as T
        else throw IllegalArgumentException("ViewModel not Found")
    }

}