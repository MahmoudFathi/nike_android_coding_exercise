package com.example.nikecodeassessment.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.nikecodeassessment.R
import com.example.nikecodeassessment.model.X
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_term.*

/**
 * Adapter for the MainActivity's RecyclerView
 */
class DataAdapter(private val data:List<X>): RecyclerView.Adapter<RecyclerView.ViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val dataView = inflater.inflate(R.layout.item_term,parent,false)
        return DataViewHolder(dataView)
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val pos = data[position]
        (holder as DataViewHolder).bind(pos)
    }
}
/**
 * ViewHolder for each item in the MainActivity's RecyclerView
 */
class DataViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(data: X) {
        val authorName = "Author:${data.author}"
        tvAuthor.text = authorName
        tvDefinition.text = data.definition
        val thumbsDown = "Thumbs Down: ${data.thumbsDown}"
        tvThumbsDown.text = thumbsDown
        val thumbsUp = "Thumbs Up: ${data.thumbsUp}"
        tvThumbsUp.text= thumbsUp
    }
}