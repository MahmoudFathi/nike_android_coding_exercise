package com.example.nikecodeassessment.model


import com.google.gson.annotations.SerializedName

data class JsonResponse(
    @SerializedName("list")
    val list: List<X>
)