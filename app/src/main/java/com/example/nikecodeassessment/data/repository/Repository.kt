package com.example.nikecodeassessment.data.repository

import com.example.nikecodeassessment.model.JsonResponse

interface Repository {
    suspend fun getDataAsync(term: String): JsonResponse
}