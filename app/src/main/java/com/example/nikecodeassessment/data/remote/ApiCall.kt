package com.example.nikecodeassessment.data.remote

import android.content.Context
import com.example.nikecodeassessment.common.BASE_URL
import com.example.nikecodeassessment.model.JsonResponse
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query
/**
 * Retrofit service interface for API calls
 */
interface ApiCall {
    @Headers(
        "X-RapidAPI-Host:mashape-community-urban-dictionary.p.rapidapi.com",
        "X-RapidAPI-Key:"
    )
    @GET("define?")
    suspend fun getData(@Query("term") term: String): JsonResponse





    /**
     * Creates and configures a retrofit adapter based on the service class
     */
    companion object{
        fun create(context: Context): ApiCall{

            val interceptor = HttpLoggingInterceptor()

            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val cacheSize = (5 * 1024*1024).toLong()

            val myCache = Cache(context.cacheDir,cacheSize)

            val okHttpClient = OkHttpClient.Builder().cache(myCache)
                .addInterceptor(interceptor).addInterceptor {chain ->
                    val request = chain.request()
                    request.newBuilder()
                        .addHeader("Cache-Control", "public, max-age=5")
                        .build()
                    chain.proceed(request)  }.build()


            val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL).build()

            return retrofit.create(ApiCall::class.java)
        }
    }
}