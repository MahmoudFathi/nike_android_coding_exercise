package com.example.nikecodeassessment.data.repository

import com.example.nikecodeassessment.data.remote.ApiCall

class RepositoryImpl(private val apiCall: ApiCall):Repository {
    override suspend fun getDataAsync(term: String) = apiCall.getData(term)
    }


