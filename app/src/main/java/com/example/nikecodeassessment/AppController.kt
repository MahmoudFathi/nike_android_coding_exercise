package com.example.nikecodeassessment

import android.app.Application
import com.example.nikecodeassessment.di.application.ApplicationComponent
import com.example.nikecodeassessment.di.application.ApplicationModule
import com.example.nikecodeassessment.di.application.DaggerApplicationComponent

class AppController : Application() {
    val applicationComponent:ApplicationComponent by lazy {
        DaggerApplicationComponent.builder().applicationModule(ApplicationModule(this.applicationContext)).build()
    }
}