package com.example.nikecodeassessment.common

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar
/**
 * Extension functions to used over the app
 */
fun snackbar(message: String, action: String, rootLayout: ViewGroup) {
    Snackbar.make(
        rootLayout,
        message, Snackbar.LENGTH_SHORT
    )
        .setAction(action) {}
        .show()
}

fun hideKeyboard(view: View){
    val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun showKeyboard(view: View){
    val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0)
}