package com.example.nikecodeassessment

import android.content.Intent
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.intent.Intents
import androidx.test.filters.LargeTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.rule.ActivityTestRule
import com.example.nikecodeassessment.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
@LargeTest
class MainActivityTest {
    @get:Rule
    val activityRule: ActivityTestRule<MainActivity> = ActivityTestRule(
        MainActivity::class.java,
        false,
        true
    )

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register()
        activityRule.launchActivity(Intent())
        Intents.init()
    }

    @Test
    fun recyclerView_isNotEmpty(){
        val rvCount = activityRule.activity.rvSearch.adapter?.itemCount ?: 0
        assert(rvCount > 0)
    }


    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister()
        Intents.release()
    }
}