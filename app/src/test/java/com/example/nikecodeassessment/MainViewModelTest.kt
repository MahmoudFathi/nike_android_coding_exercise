package com.example.nikecodeassessment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.nikecodeassessment.data.repository.RepositoryImpl
import com.example.nikecodeassessment.model.X
import com.example.nikecodeassessment.ui.MainViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Spy
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class MainViewModelTest {
    @Rule
    @JvmField
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var mockRepository: RepositoryImpl

    @Mock
    private lateinit var mockDataObserver: Observer<List<X>>

    @Spy
    private var liveData: MutableLiveData<List<X>> = MutableLiveData()
    lateinit var mainViewModel: MainViewModel


    @Before
    fun setUp() {
        liveData.value = listOf(
            X("","",1,"","","", listOf(Any()),10,20,"game",""),
            X("","",2,"","","", listOf(Any()),15,25,"game",""),
            X("","",3,"","","", listOf(Any()),17,27,"game","")
        )
        mainViewModel = MainViewModel(mockRepository,liveData)
    }

    @Test
    fun sortBy_thumbsdown(){
        val result =  mainViewModel.loadDataMostDown()
        assert(true) { result }
    }

    @Test
    fun sortBy_thumbsUp(){
        val result = mainViewModel.loadDataMostUp()
        assert(true){result}
    }


    @After
    fun tearDown(){
        mainViewModel.livedata.removeObserver(mockDataObserver)

    }

}